<?php

namespace Drupal\user_revision_diff\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\diff\Controller\PluginRevisionController;
use Drupal\user\UserInterface;

/**
 * Returns responses for User revision routes.
 *
 * @see \Drupal\diff\Controller\NodeRevisionController
 */
class UserRevisionController extends PluginRevisionController implements ContainerInjectionInterface {

  /**
   * Returns a form for user overview page.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user whose revisions are inspected.
   *
   * @return array
   *   Render array containing the revisions table for $node.
   *
   * @see \Drupal\diff\Controller\NodeRevisionController::revisionOverview()
   * @todo This might be changed to a view when the issue at this link is
   *   resolved: https://drupal.org/node/1863906
   */
  public function revisionOverview(UserInterface $user) {
    return $this->formBuilder()
      ->getForm('Drupal\user_revision_diff\Form\RevisionOverviewForm', $user);
  }

  /**
   * Returns a table which shows the differences between two user revisions.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user whose revisions are compared.
   * @param int $left_revision
   *   Vid of the node revision from the left.
   * @param int $right_revision
   *   Vid of the node revision from the right.
   * @param string $filter
   *   If $filter == 'raw' raw text is compared (including html tags)
   *   If $filter == 'raw-plain' markdown function is applied to the text
   *   before comparison.
   *
   * @return array
   *   Table showing the diff between the two user revisions.
   *
   * @see \Drupal\diff\Controller\NodeRevisionController
   */
  public function compareUserRevisions(UserInterface $user, $left_revision, $right_revision, $filter) {
    $storage = $this->entityTypeManager()->getStorage('user');
    $route_match = \Drupal::routeMatch();
    $left_revision = $storage->loadRevision($left_revision);
    $right_revision = $storage->loadRevision($right_revision);
    $build = $this->compareEntityRevisions($route_match, $left_revision, $right_revision, $filter);
    return $build;
  }

}
